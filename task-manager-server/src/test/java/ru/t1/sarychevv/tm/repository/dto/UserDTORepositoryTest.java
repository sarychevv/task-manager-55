package ru.t1.sarychevv.tm.repository.dto;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.sarychevv.tm.api.repository.dto.IUserDTORepository;
import ru.t1.sarychevv.tm.api.service.IConnectionService;
import ru.t1.sarychevv.tm.api.service.IPropertyService;
import ru.t1.sarychevv.tm.api.service.dto.IProjectDTOService;
import ru.t1.sarychevv.tm.api.service.dto.ITaskDTOService;
import ru.t1.sarychevv.tm.api.service.dto.IUserDTOService;
import ru.t1.sarychevv.tm.dto.model.UserDTO;
import ru.t1.sarychevv.tm.enumerated.Role;
import ru.t1.sarychevv.tm.repository.migration.AbstractSchemeTest;
import ru.t1.sarychevv.tm.service.ConnectionService;
import ru.t1.sarychevv.tm.service.PropertyService;
import ru.t1.sarychevv.tm.service.dto.ProjectDTOService;
import ru.t1.sarychevv.tm.service.dto.TaskDTOService;
import ru.t1.sarychevv.tm.service.dto.UserDTOService;

import javax.persistence.EntityManager;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class UserDTORepositoryTest extends AbstractSchemeTest {

    @NotNull
    public final static UserDTO USER_TEST = new UserDTO();

    @NotNull
    public final static UserDTO ADMIN_TEST = new UserDTO();
    @NotNull
    public final static List<UserDTO> USER_LIST = Arrays.asList(USER_TEST, ADMIN_TEST);
    @NotNull
    private static final IPropertyService propertyService = new PropertyService();
    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);
    @NotNull
    private static final IProjectDTOService projectService = new ProjectDTOService(connectionService);
    @NotNull
    private static final ITaskDTOService taskService = new TaskDTOService(connectionService);
    @NotNull
    private static final IUserDTOService userService = new UserDTOService(propertyService, connectionService, projectService, taskService);

    static {
        USER_TEST.setPassword("user_test_password");
        USER_TEST.setLogin("user_test_login");
        USER_TEST.setRole(Role.USUAL);
        USER_TEST.setEmail("user_test_email");
        USER_TEST.setFirstName("user_test_first_name");
        USER_TEST.setLastName("user_test_last_name");
        USER_TEST.setMiddleName("user_test_middle_name");

        ADMIN_TEST.setPassword("admin_test_password");
        ADMIN_TEST.setLogin("admin_test_login");
        ADMIN_TEST.setRole(Role.ADMIN);
        ADMIN_TEST.setEmail("admin_test_email");
        ADMIN_TEST.setFirstName("admin_test_first_name");
        ADMIN_TEST.setLastName("admin_test_last_name");
        ADMIN_TEST.setMiddleName("admin_test_middle_name");
    }

    @NotNull
    private static IUserDTORepository getRepository(@NotNull final EntityManager entityManager) {
        return new UserDTORepository(entityManager);
    }

    @NotNull
    private static EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @BeforeClass
    public static void initRepository() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @Before
    public void setUp() throws Exception {
        @NotNull final UserDTO user = new UserDTO();
        user.setPassword("user_password");
        user.setLogin("user_login");
        user.setRole(Role.USUAL);
        user.setEmail("user_email");
        user.setFirstName("user_first_name");
        user.setLastName("user_last_name");
        user.setMiddleName("user_middle_name");
        userService.add(user);

        @NotNull final UserDTO admin = new UserDTO();
        admin.setPassword("admin_password");
        admin.setLogin("admin_login");
        admin.setRole(Role.ADMIN);
        admin.setEmail("admin_email");
        admin.setFirstName("admin_first_name");
        admin.setLastName("admin_last_name");
        admin.setMiddleName("admin_middle_name");
        userService.add(admin);

        userService.add(USER_TEST);
        userService.add(ADMIN_TEST);
    }

    @After
    public void tearDown() throws Exception {
        @Nullable final UserDTO user = userService.findByLogin("user_test_login");
        if (user != null) userService.removeOne(user);

        @Nullable final UserDTO admin = userService.findByLogin("admin_test_login");
        if (user != null) userService.removeOne(admin);
    }

    @Test
    public void testAdd() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertNotNull(repository.add(ADMIN_TEST));
            entityManager.getTransaction().commit();
            @Nullable final UserDTO user = repository.findOneById(ADMIN_TEST.getId());
            Assert.assertNotNull(user);
            Assert.assertEquals(ADMIN_TEST.getId(), user.getId());
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testExistsById() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserDTORepository repository = getRepository(entityManager);
        Assert.assertFalse(repository.existsById(UUID.randomUUID().toString()));
        Assert.assertTrue(repository.existsById(USER_TEST.getId()));
        entityManager.close();
    }

    @Test
    public void testFindAll() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserDTORepository repository = getRepository(entityManager);
        List<UserDTO> users = repository.findAll();
    }

    @Test
    public void testFindOneById() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserDTORepository repository = getRepository(entityManager);
        Assert.assertNull(repository.findOneById(UUID.randomUUID().toString()));
        @Nullable final UserDTO user = repository.findOneById(USER_TEST.getId());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST.getId(), user.getId());
        entityManager.close();
    }

    @Test
    public void testGetSize() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserDTORepository repository = getRepository(entityManager);
    }

    @Test
    public void testRemoveOne() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = getRepository(entityManager);
            @NotNull UserDTO user = new UserDTO();
            user.setPassword("user_remove_test_password");
            user.setLogin("user_remove_test_login");
            user.setRole(Role.USUAL);
            user.setEmail("user_remove_test_email");
            user.setFirstName("user_remove_test_first_name");
            user.setLastName("user_remove_test_last_name");
            user.setMiddleName("user_remove_test_middle_name");
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
            Assert.assertNotNull(repository.findByEmail(user.getEmail()));
            entityManager.getTransaction().begin();
            repository.removeOne(user);
            entityManager.getTransaction().commit();
            Assert.assertNull(repository.findByEmail(user.getEmail()));
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testUpdate() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = getRepository(entityManager);
            @NotNull UserDTO user = new UserDTO();
            user.setPassword("user_update_test_password");
            user.setLogin("user_update_test_login");
            user.setRole(Role.USUAL);
            user.setEmail("user_update_test_email");
            user.setFirstName("user_update_test_first_name");
            user.setLastName("user_update_test_last_name");
            user.setMiddleName("user_update_test_middle_name");
            entityManager.getTransaction().begin();
            repository.add(user);
            entityManager.getTransaction().commit();
            entityManager.getTransaction().begin();
            user.setEmail("user_updated_update_test_email");
            repository.update(user);
            entityManager.getTransaction().commit();
            Assert.assertEquals(user.getEmail(), repository.findByLogin(user.getLogin()).getEmail());
            entityManager.getTransaction().begin();
            repository.removeOne(repository.findByLogin(user.getLogin()));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testCreate() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            @NotNull final UserDTO user = repository.create("user_create_test_login", "user_create_test_password");
            Assert.assertEquals("user_create_test_login", user.getLogin());
            Assert.assertEquals("user_create_test_password", user.getPassword());
            entityManager.getTransaction().commit();
            entityManager.getTransaction().begin();
            repository.removeOne(user);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    public void testFindByLogin() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserDTORepository repository = getRepository(entityManager);
        @Nullable final UserDTO user = repository.findByLogin(USER_TEST.getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST.getId(), user.getId());
        entityManager.close();
    }

    @Test
    public void testFindByEmail() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserDTORepository repository = getRepository(entityManager);
        @Nullable final UserDTO user = repository.findByEmail(USER_TEST.getEmail());
        Assert.assertNotNull(user);
        Assert.assertEquals(USER_TEST.getId(), user.getId());
        entityManager.close();
    }

    @Test
    public void testIsLoginExists() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserDTORepository repository = getRepository(entityManager);
        Assert.assertTrue(repository.isLoginExists(USER_TEST.getLogin()));
        entityManager.close();
    }

    @Test
    public void testIsEmailExists() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        @NotNull final IUserDTORepository repository = getRepository(entityManager);
        Assert.assertTrue(repository.isEmailExists(USER_TEST.getEmail()));
        entityManager.close();
    }

    @Test
    public void testRemoveAll() throws Exception {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final IUserDTORepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.removeAll();
            entityManager.getTransaction().commit();
            Assert.assertEquals(repository.getSize(), 0);
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
        } finally {
            entityManager.close();
        }
    }
}