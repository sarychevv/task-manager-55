package ru.t1.sarychevv.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.api.repository.model.ISessionRepository;
import ru.t1.sarychevv.tm.model.Session;

import javax.persistence.EntityManager;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    private static final Class<Session> type = null;

    public SessionRepository(@NotNull final EntityManager entityManager) {
        super(entityManager, type);
    }

}

