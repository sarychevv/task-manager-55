package ru.t1.sarychevv.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.api.repository.model.IProjectRepository;
import ru.t1.sarychevv.tm.model.Project;

import javax.persistence.EntityManager;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    private static final Class<Project> type = null;

    public ProjectRepository(@NotNull final EntityManager entityManager) {
        super(entityManager, type);
    }

    @NotNull
    @Override
    public Project create(@NotNull final String userId,
                          @NotNull final String name,
                          @NotNull final String description) throws Exception {
        @NotNull final Project project = new Project(name, description);
        return add(userId, project);
    }

    @NotNull
    @Override
    public Project create(@NotNull final String userId,
                          @NotNull final String name) throws Exception {
        @NotNull final Project project = new Project(name);
        return add(userId, project);
    }

}

