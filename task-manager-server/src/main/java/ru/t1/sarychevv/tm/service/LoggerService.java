package ru.t1.sarychevv.tm.service;

import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.internal.SessionFactoryImpl;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.api.service.IConnectionService;
import ru.t1.sarychevv.tm.api.service.ILoggerService;
import ru.t1.sarychevv.tm.api.service.IPropertyService;
import ru.t1.sarychevv.tm.listener.EntityListener;
import ru.t1.sarychevv.tm.listener.JmsLoggerProducer;

import javax.persistence.EntityManagerFactory;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.*;

public final class LoggerService implements ILoggerService {

    @NotNull
    private static final String CONFIG_FILE = "/logger.properties";
    @NotNull
    private static final String COMMANDS = "COMMANDS";
    @NotNull
    private static final String COMMANDS_FILE = "./commands.xml";
    @NotNull
    private static final String ERRORS = "ERRORS";
    @NotNull
    private static final String ERRORS_FILE = "./errors.xml";
    @NotNull
    private static final String MESSAGES = "MESSAGES";
    @NotNull
    private static final String MESSAGES_FILE = "./messages.xml";
    @NotNull
    private static final LogManager MANAGER = LogManager.getLogManager();
    @NotNull
    private static final Logger LOGGER_ROOT = Logger.getLogger("");
    @NotNull
    private static final Logger LOGGER_COMMAND = Logger.getLogger(COMMANDS);
    @NotNull
    private static final Logger LOGGER_ERROR = Logger.getLogger(ERRORS);
    @NotNull
    private static final Logger LOGGER_MESSAGE = Logger.getLogger(MESSAGES);
    @NotNull
    private static final ConsoleHandler CONSOLE_HANDLER = getConsoleHandler();

    static {
        loadConfigFromFile();
        registry(LOGGER_COMMAND, COMMANDS_FILE, false);
        registry(LOGGER_ERROR, ERRORS_FILE, true);
        registry(LOGGER_MESSAGE, MESSAGES_FILE, true);
    }

    @NotNull
    private final IConnectionService connectionService;
    @NotNull
    private final IPropertyService propertyService;

    public LoggerService(@NotNull final IConnectionService connectionService,
                         @NotNull final IPropertyService propertyService) {
        this.connectionService = connectionService;
        this.propertyService = propertyService;
    }

    private static void loadConfigFromFile() {
        try {
            @Nullable final InputStream is = LoggerService.class.getResourceAsStream(CONFIG_FILE);
            MANAGER.readConfiguration(is);
        } catch (@NotNull final IOException e) {
            LOGGER_ROOT.severe(e.getMessage());
        }
    }

    @NotNull
    private static ConsoleHandler getConsoleHandler() {
        @NotNull final ConsoleHandler handler = new ConsoleHandler();
        handler.setFormatter(new Formatter() {
            public String format(@NotNull LogRecord record) {
                return record.getMessage() + "\n";
            }
        });
        return handler;
    }

    private static void registry(@NotNull final Logger logger, @Nullable final String fileName, final boolean isConsole) {
        try {
            if (isConsole) {
                logger.addHandler(CONSOLE_HANDLER);
            }
            logger.setUseParentHandlers(false);
            if (fileName != null && !fileName.isEmpty()) {
                logger.addHandler(new FileHandler(fileName));
            }
        } catch (@NotNull final IOException e) {
            LOGGER_ROOT.severe(e.getMessage());
        }
    }

    @Override
    public void info(@Nullable final String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_MESSAGE.info(message);
    }

    @Override
    public void command(@Nullable String message) {
        if (message == null || message.isEmpty()) return;
        LOGGER_COMMAND.info(message);
    }

    @Override
    public void error(@Nullable Exception e) {
        if (e == null) return;
        LOGGER_ERROR.log(Level.SEVERE, e.getMessage(), e);
    }

    @Override
    public void startJMSLogger() {
        @NotNull final JmsLoggerProducer jmsLoggerProducer = new JmsLoggerProducer(propertyService);
        @NotNull final EntityListener entityListener = new EntityListener(jmsLoggerProducer);
        @NotNull final EntityManagerFactory entityManagerFactory = connectionService.getEntityManager().getEntityManagerFactory();
        @NotNull final SessionFactoryImpl sessionFactoryImpl = entityManagerFactory.unwrap(SessionFactoryImpl.class);
        @NotNull final EventListenerRegistry eventListenerRegistry = sessionFactoryImpl.getServiceRegistry().getService(EventListenerRegistry.class);
        eventListenerRegistry.getEventListenerGroup(EventType.POST_INSERT).appendListener(entityListener);
        eventListenerRegistry.getEventListenerGroup(EventType.POST_UPDATE).appendListener(entityListener);
        eventListenerRegistry.getEventListenerGroup(EventType.POST_DELETE).appendListener(entityListener);
    }

}
