package ru.t1.sarychevv.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.api.service.IPropertyService;

import java.util.Properties;

public final class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    private static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    private static final String SESSION_KEY = "session.key";

    @NotNull
    private static final String SESSION_TIME_OUT_KEY = "session.timeout";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "1";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "1";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String EMPTY_VALUE = "---";

    @NotNull
    private static final String DATABASE_PASSWORD_KEY = "database.password";

    @NotNull
    private static final String DATABASE_URL_KEY = "database.url";

    @NotNull
    private static final String DATABASE_USERNAME_KEY = "database.username";

    @NotNull
    private static final String DATABASE_DRIVER = "database.driver";

    @NotNull
    private static final String DATABASE_DIALECT = "database.dialect";

    @NotNull
    private static final String DATABASE_SHOW_SQL = "database.show_sql";

    @NotNull
    private static final String DATABASE_HBM2DDL_AUTO = "database.hbm2ddl_auto";

    @NotNull
    private static final String DATABASE_FORMAT_SQL = "database.format_sql";

    @NotNull
    private static final String DATABASE_SECOND_LVL_CACHE = "database.second_lvl_cache";

    @NotNull
    private static final String DATABASE_FACTORY_CLASS = "database.factory_class";

    @NotNull
    private static final String DATABASE_USE_QUERY_CACHE = "database.use_query_cache";

    @NotNull
    private static final String DATABASE_USE_MIN_PUTS = "database.use_min_puts";

    @NotNull
    private static final String DATABASE_REGION_PREFIX = "database.region_prefix";

    @NotNull
    private static final String DATABASE_CONFIG_FILE_PATH = "database.config_file_path";

    @NotNull
    private static final String JMS_QUEUE = "jms.queue";

    @NotNull
    private static final String JMS_CONNECTOR = "jms.connector";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @Override
    @NotNull
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @Override
    @NotNull
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @Override
    @NotNull
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @Override
    @NotNull
    public Integer getServerPort() {
        return getIntegerValue(SERVER_PORT_KEY, EMPTY_VALUE);
    }

    @Override
    @NotNull
    public String getServerHost() {
        return getStringValue(SERVER_HOST_KEY, EMPTY_VALUE);
    }

    @Override
    @NotNull
    public String getSessionKey() {
        return getStringValue(SESSION_KEY);
    }

    @Override
    @NotNull
    public Integer getSessionTimeOut() {
        return getIntegerValue(SESSION_TIME_OUT_KEY, EMPTY_VALUE);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    public Integer getPasswordIteration() {
        return getIntegerValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @Override
    public @NotNull String getDatabasePassword() {
        return getStringValue(DATABASE_PASSWORD_KEY);
    }

    @Override
    public @NotNull String getDatabaseUrl() {
        return getStringValue(DATABASE_URL_KEY);
    }

    @Override
    public @NotNull String getDatabaseUsername() {
        return getStringValue(DATABASE_USERNAME_KEY);
    }

    @NotNull
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    private Integer getIntegerValue(@NotNull final String key, @NotNull final String defaultValue) {
        return Integer.parseInt(getStringValue(key, defaultValue));
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBDriver() {
        return getStringValue(DATABASE_DRIVER, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBDialect() {
        return getStringValue(DATABASE_DIALECT, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBShowSql() {
        return getStringValue(DATABASE_SHOW_SQL, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBHbm2ddlAuto() {
        return getStringValue(DATABASE_HBM2DDL_AUTO, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBFormatSql() {
        return getStringValue(DATABASE_FORMAT_SQL, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBSecondLvlCache() {
        return getStringValue(DATABASE_SECOND_LVL_CACHE, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBFactoryClass() {
        return getStringValue(DATABASE_FACTORY_CLASS, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBUseQueryCache() {
        return getStringValue(DATABASE_USE_QUERY_CACHE, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBUseMinPuts() {
        return getStringValue(DATABASE_USE_MIN_PUTS, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBRegionPrefix() {
        return getStringValue(DATABASE_REGION_PREFIX, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getDBConfigFilePath() {
        return getStringValue(DATABASE_CONFIG_FILE_PATH, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getJmsQueue() {
        return getStringValue(JMS_QUEUE, EMPTY_VALUE);
    }

    @NotNull
    @Override
    public String getJmsConnector() {
        return getStringValue(JMS_CONNECTOR, EMPTY_VALUE);
    }

}
