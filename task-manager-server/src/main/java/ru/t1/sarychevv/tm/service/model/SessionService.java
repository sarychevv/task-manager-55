package ru.t1.sarychevv.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.api.repository.model.ISessionRepository;
import ru.t1.sarychevv.tm.api.service.IConnectionService;
import ru.t1.sarychevv.tm.api.service.model.ISessionService;
import ru.t1.sarychevv.tm.exception.entity.TaskNotFoundException;
import ru.t1.sarychevv.tm.exception.field.*;
import ru.t1.sarychevv.tm.model.Session;
import ru.t1.sarychevv.tm.repository.model.SessionRepository;

import javax.persistence.EntityManager;

public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    protected ISessionRepository getRepository(@NotNull final EntityManager entityManager) {
        return new SessionRepository(entityManager);
    }

    @NotNull
    @Override
    public Session updateById(@Nullable final String userId,
                              @Nullable final String id,
                              @Nullable final String name,
                              @Nullable final String description) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Session session = findOneById(userId, id);
        if (session == null) throw new TaskNotFoundException();
        session.setName(name);
        session.setDescription(description);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ISessionRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(userId, session);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return session;
    }

    @NotNull
    @Override
    public Session updateByIndex(@Nullable final String userId,
                                 @Nullable final Integer index,
                                 @Nullable final String name,
                                 @Nullable final String description) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Session session = findOneByIndex(userId, index);
        if (session == null) throw new TaskNotFoundException();
        session.setName(name);
        session.setDescription(description);
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ISessionRepository repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(userId, session);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return session;
    }

}

