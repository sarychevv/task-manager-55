# TASK MANAGER

## DEVELOPER INFO

**NAME**: Vladimir Sarychev

**EMAIL**: vsarychev@t1-consulting.ru

**EMAIL**: vsarychev@tsconsulting.com

## SOFTWARE

**JAVA**: OPENJDK 1.8

**OS**: WINDOWS 10 PRO 21H2

## HARDWARE

**CPU**: E5-1650 v2

**RAM**: 16GB

**SDD**: 512GB

## BUILD PROGRAM

```
mvn clean install
```

## RUN PROGRAM

```
java -jar ./task-manager.jar
```
