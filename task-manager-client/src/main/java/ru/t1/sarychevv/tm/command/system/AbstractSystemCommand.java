package ru.t1.sarychevv.tm.command.system;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.api.service.ICommandService;
import ru.t1.sarychevv.tm.api.service.ILoggerService;
import ru.t1.sarychevv.tm.api.service.IPropertyService;
import ru.t1.sarychevv.tm.command.AbstractCommand;

@Getter
@Setter
@Component
public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    @Autowired
    public ICommandService commandService;

    @NotNull
    @Autowired
    public IPropertyService propertyService;

    @NotNull
    @Autowired
    public ILoggerService loggerService;

}
