package ru.t1.sarychevv.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.dto.request.user.UserUnlockRequest;
import ru.t1.sarychevv.tm.util.TerminalUtil;

@Component
public class UserUnlockCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "User unlock";
    }

    @NotNull
    @Override
    public String getName() {
        return "user-unlock";
    }

    @Override
    public void execute() {
        System.out.println("[USER UNLOCK]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        @NotNull final UserUnlockRequest request = new UserUnlockRequest(getToken());
        request.setLogin(login);
        getUserEndpoint().unlockUser(request);
    }

}
