package ru.t1.sarychevv.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.dto.model.TaskDTO;
import ru.t1.sarychevv.tm.dto.request.task.TaskGetByIndexRequest;
import ru.t1.sarychevv.tm.util.TerminalUtil;

@Component
public class TaskShowByIndexCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Show task by index.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-show-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskGetByIndexRequest request = new TaskGetByIndexRequest(getToken());
        request.setIndex(index);
        @Nullable final TaskDTO task = getTaskEndpoint().getTaskByIndex(request).getTask();
        showTask(task);
    }

}
