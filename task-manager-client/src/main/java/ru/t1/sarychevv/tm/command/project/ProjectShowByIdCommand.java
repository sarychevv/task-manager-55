package ru.t1.sarychevv.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.dto.model.ProjectDTO;
import ru.t1.sarychevv.tm.dto.request.project.ProjectGetByIdRequest;
import ru.t1.sarychevv.tm.util.TerminalUtil;

@Component
public class ProjectShowByIdCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Show project by id.";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-show-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final ProjectGetByIdRequest request = new ProjectGetByIdRequest(getToken());
        request.setId(id);
        @Nullable final ProjectDTO project = getProjectEndpoint().getProjectById(request).getProject();
        showProject(project);
    }

}
