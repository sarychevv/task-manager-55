package ru.t1.sarychevv.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.dto.model.TaskDTO;
import ru.t1.sarychevv.tm.dto.request.task.TaskGetByIdRequest;
import ru.t1.sarychevv.tm.util.TerminalUtil;

@Component
public class TaskShowByIdCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Show task by id.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-show-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final TaskGetByIdRequest request = new TaskGetByIdRequest(getToken());
        request.setId(id);
        @Nullable final TaskDTO task = getTaskEndpoint().getTaskById(request).getTask();
        showTask(task);
    }

}
