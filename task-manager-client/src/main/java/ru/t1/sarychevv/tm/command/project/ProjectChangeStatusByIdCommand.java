package ru.t1.sarychevv.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.dto.request.project.ProjectChangeStatusByIdRequest;
import ru.t1.sarychevv.tm.enumerated.Status;
import ru.t1.sarychevv.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public class ProjectChangeStatusByIdCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Change project status by id";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-change-status-by-id";
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PROJECT STATUS BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @Nullable final String statusValue = TerminalUtil.nextLine();
        @Nullable final Status status = Status.toStatus(statusValue);
        @NotNull final ProjectChangeStatusByIdRequest request = new ProjectChangeStatusByIdRequest(getToken());
        request.setId(id);
        request.setStatus(status);
        getProjectEndpoint().changeProjectStatusById(request);
    }

}
