package ru.t1.sarychevv.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractWorkModel extends AbstractUserOwnedModel {

    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    protected Status status = Status.NOT_STARTED;

}
