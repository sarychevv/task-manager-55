package ru.t1.sarychevv.tm.dto.request.system;

import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.request.AbstractUserRequest;

public final class DropSchemeRequest extends AbstractUserRequest {

    public DropSchemeRequest(@Nullable final String token) {
        super(token);
    }

}
